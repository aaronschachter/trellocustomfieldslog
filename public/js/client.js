'use strict';

const Promise = TrelloPowerUp.Promise;

// Icon made by https://www.flaticon.com/authors/designmodo from www.flaticon.com 
const CALENDAR_ICON = 'https://cdn.glitch.com/ae15eb79-828d-47cf-bd34-a4ad209fd7e8%2Fsmall-calendar.png?1527744499148';

TrelloPowerUp.initialize({
	'card-detail-badges': (t, options) => {
    const context = t.getContext();
    return t.card('all')
      .then((card) => {
        if (!card.customFieldItems.length) {
          return [];
        }

        return [{
          icon: CALENDAR_ICON,
          title: 'Custom Fields Log',
          text: 'View changelog',
          callback: (t) => {
            return t.popup({
              title: "Custom Fields Log",
              url: 'customFieldsLog.html',
            });
          }
        }];
      });
	},
});
