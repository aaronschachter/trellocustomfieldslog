'use strict';

const t = TrelloPowerUp.iframe();
const context = t.getContext();

const config = {
  cacheKey: 'customFieldsLog',
  debug: false,
  proxyPath: 'api',
  actionType: 'updateCustomFieldItem',
};

/**
 * @param {String|Object} message
 */
function debug(message) {
  if (config.debug) console.log(message);
}

/**
 * @param {String}
 * @return {String}
 */
function formatDateString(dateString) {
  const date = new Date(dateString);
  const result = `${(date.getMonth() + 1)}/${date.getDate()}/${date.getFullYear()}`;
  return result;
}

/**
 * @param {String}
 * @return {String}
 */
function getProxyPath(path) {
  return `/${config.proxyPath}/${path}`;
}

/**
 * @param {String} cardId
 * @return {Promise}
 */
function fetchCustomFieldsLogActionsByCardId(cardId) {
  // TODO: It'd be sweet to pass a query param to filter by action type.
  const endpoint = getProxyPath(`cards/${cardId}/actions?filter=all`);
  return fetch(endpoint)
    .then(res => res.json())
    .then(actions => actions.filter(action => action.type === config.actionType));
}

/**
 * @param {String} boardId
 * @return {Promise}
 */
function fetchCustomFieldOptionsByBoardId(boardId) {
  const endpoint = getProxyPath(`boards/${boardId}/customFields`);
  return fetch(endpoint)
    .then(res => res.json())
    .then((customFields) => {
      const data = {};
      customFields.filter(customField => customField.type === 'list').forEach((customField) => {
        data[option.id] = option.value.text;
      });
      return data;
    });
}

/**
 * @param {Object} customFieldItem
 * @param {Object} customFieldOptions
 * @return {String}
 */
function getValueFromCustomFieldItemAndOptions(customFieldItem, customFieldOptions) {
  const value = customFieldItem.value;

  if (customFieldItem.idValue) {
    const optionValue = customFieldOptions[customFieldItem.idValue];
    return optionValue ? optionValue : '<em>field deleted</em>';
  }

  if (!value) {
    return '-';
  }

  if (value.date) {
    return formatDateString(value.date);
  }

  if (value.number) {
    return value.number;
  }

  if (value.checked) {
    return value.checked === 'true' ? 'checked' : 'unchecked';
  }

  return value.text;
}

/**
 * @param {Object} action
 * @param {Object} customFieldOptions
 * @return {Object}
 */
function parseCustomFieldsLogItemFromActionAndOptions(action, customFieldOptions) {
  return {
    name: action.data.customField.name,
    value: getValueFromCustomFieldItemAndOptions(action.data.customFieldItem, customFieldOptions),
    date: action.date,
  };
}

/**
 * @param {String} html
 */
function renderHtml(html) {
  window.changelog.innerHTML = html;
  t.sizeTo('#changelog').done();
}

/**
 * @param {Array} customFieldsLogItems
 */
function renderCustomFieldsLogItems(customFieldsLogItems) {
  const listItems = customFieldsLogItems.map((logItem) => {
    return `<li>${logItem.name}: ${logItem.value}<p><small>Updated on ${formatDateString(logItem.date)}</small></p></li>`;
  });
  renderHtml(listItems.join(''));
}

t.render(() => {
  let customFieldOptions = {};
  return  t.get('card', 'shared', config.cacheKey)
    .then((customFieldsLogItems) => {
      // Display cached version first if exists. 
      if (customFieldsLogItems) {
        debug('cache hit');
        renderCustomFieldsLogItems(customFieldsLogItems);
      } else {
        debug('cache miss');
        renderHtml('Loading...');
      }
      return fetchCustomFieldOptionsByBoardId(context.board);
    })
    .then((data) => {
      customFieldOptions = data;
      debug('fetchCustomFieldOptionsByBoardId success');
      debug(data);
      return fetchCustomFieldsLogActionsByCardId(context.card);
    })
    .then((actions) => {
      debug('fetchCustomFieldsLogActionsByCardId success');
      debug(actions);
      const customFieldsLogItems = actions
        .map(action => parseCustomFieldsLogItemFromActionAndOptions(action, customFieldOptions));
      renderCustomFieldsLogItems(customFieldsLogItems);
      return t.set('card', 'shared', config.cacheKey, customFieldsLogItems);
    })
    .catch((err) => {
      const errorHtml = `<h3>Error :(</h3><p>Try refreshing your browser to see if the problem persists. <a href="https://www.trellostatus.com/" target="_blank">Check Trello status</a></p><code>${err.message}</code>`;
      renderHtml(errorHtml);
    });
});
