'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const proxy = require('http-proxy-middleware');

const app = express();
const router = express.Router()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(cors({ origin: '*' }));
app.use(express.static('public'));

app.use(router);

app.get('/', (request, response) => {
  response.sendFile(__dirname + '/views/index.html');
});

const addTrelloAuth = (path) => {
  const authString = `key=${process.env.TRELLO_API_KEY}&token=${process.env.TRELLO_API_TOKEN}`;
  let separator = '?';
  if (path.includes(separator)) {
    separator = '&';
  }
  const result = `${path}${separator}${authString}`.replace('api/', '');
  return result;
}

const options = {
  target: 'https://api.trello.com/1',
  changeOrigin: true,
  pathRewrite: addTrelloAuth
};

app.use('/api', proxy('/api', options));

// listen for requests :)
const listener = app.listen(process.env.PORT, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});

