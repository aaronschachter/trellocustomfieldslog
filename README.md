# Trello Custom Fields Log Power-Up 


This is a MVP for a Trello Power-Up that displays a changelog of Custom Field values to a card if it has any Custom Fields set.

I use it for a meal planning board, where I store meals my family eats as cards, in lists like "Homecooked", "Takeout ", and "Sitdown". When we have a meal again, I'll update a date Custom Field called "Last Occurred" to keep track of the last time we had it (and hopefully surface oldies but goodies where it's been a while since we've had it -- for some variety :pizza:)

Roadmap:

* Option to sort a list by a current Custom Field value

* Calendar view of cards by dates that a Custom Field value changed
